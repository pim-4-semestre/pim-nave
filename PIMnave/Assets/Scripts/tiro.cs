﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tiro : MonoBehaviour
{
    float velocidadeTiro = 5f;
    void Start() {
        StartCoroutine("sumirTiro");
    }
    void Update()
    {
        transform.position += Vector3.up * velocidadeTiro * Time.deltaTime;
    }

    IEnumerator sumirTiro() {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }

    public void setVelocidadeTiro(float velocidade) {
        velocidadeTiro = velocidade;
    }
}
