﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    public GameObject nave;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("spawnar");
    }

    IEnumerator spawnar() 
    {
        for(int i = 0; i <= 5; i++) 
        {
            yield return new WaitForSeconds(1f);
            Instantiate(nave, new Vector3(Random.Range(-4f, 4f), 5, 0), Quaternion.identity);
            if(i == 5) {
                yield return new WaitForSeconds(2f);
                i = 0;
            }
        }
    }
}
