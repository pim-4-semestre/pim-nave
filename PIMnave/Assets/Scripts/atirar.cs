﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class atirar : MonoBehaviour
{
    public GameObject tiro;
    float proxTiro = 0;
    
    void Update()
    {
        spawnarTiro();
    }

    void spawnarTiro() 
    {
        if(proxTiro < Time.time) {
            var tiroAtual = Instantiate(tiro, transform.position, Quaternion.identity);
            tiroAtual.GetComponent<tiro>().setVelocidadeTiro(40f);
            proxTiro = Time.time + 1f;
        }
    }
}
