﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inimigo : MonoBehaviour
{
    void Update()
    {
        transform.position += Vector3.down * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D col) {
        if(col.gameObject.layer == 8) {
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
    }
}
