﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimento : MonoBehaviour
{
    
    Rigidbody2D rb;
    Vector2 posInicial, posFinal;
    Vector3 temp;
    public float velocidadeTiro = 10f;
    bool controleSwipe = true;

    void Start() {
        rb = GetComponent<Rigidbody2D>();
    }
    public void Update() {
        if(controleSwipe) 
        {
            swipe();
        } else {
            if(Input.GetKey(KeyCode.Mouse0)) {
                temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                temp.y = -4f;
                temp.z = 0f;
                transform.position = temp;
            }
            
        }
    }

    void swipe() 
    {
        if(Input.GetKey(KeyCode.Mouse0)) 
        {
            if(Input.GetKeyDown(KeyCode.Mouse0)) {
                posInicial = Input.mousePosition;
            }

            posFinal = Input.mousePosition;

            Debug.Log(posFinal.x);
        }

        if(posInicial.x - posFinal.x > 0) 
        {
            rb.velocity = -transform.right * 4;
        } else if(posInicial.x - posFinal.x < 0) 
        {
            rb.velocity = transform.right * 4;
        }

        if(Input.GetKeyUp(KeyCode.Mouse0)) {
            posInicial = new Vector2(0, 0);
            posFinal = new Vector2(0, 0);
            rb.velocity = new Vector2(0, 0);
        }

        Debug.Log(Mathf.Abs(posInicial.x - posFinal.x));
    }

    public void trocarControle() {
        controleSwipe = !controleSwipe;
        rb.velocity = new Vector2(0, 0);
    }
}
