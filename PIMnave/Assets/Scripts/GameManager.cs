﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instancia = null;

    void Awake() {
        if(instancia == null) {
            instancia = this;
        }

        else if(instancia != this) {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
}
